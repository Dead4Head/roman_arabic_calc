package org.amsmirnov;

enum Romans {

    EMPTY("O"),
    I("I"),
    II("II"),
    III("III"),
    IV("IV"),
    V("V"),
    VI("VI"),
    VII("VII"),
    VIII("VIII"),
    IX("IX"),
    X("X");

    private String title;

    Romans(final String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return title;
    }

}
