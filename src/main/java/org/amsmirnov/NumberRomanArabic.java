package org.amsmirnov;

/**
 * Если введенное число не определяется как римское или арабское будет {@link IllegalArgumentException}
 * что прервет выполнение программы т.к. это RTE
 */
class NumberRomanArabic {

    private final int value;
    private final boolean arabic;

    public NumberRomanArabic(final int value, final boolean arabic){
        this.value = value;
        this.arabic = arabic;
    }

    public NumberRomanArabic(final String str){
        int value;
        boolean arabic;
        try {
            value = Integer.parseInt(str);
            arabic = true;
        } catch (NumberFormatException ex) {
            value = Romans.valueOf(str).ordinal();
            arabic = false;
        }
        this.value = value;
        if (value < 1 || value > 10)
            throw  new RuntimeException("Error! Calculator can work only with integers in [1; 10]");
        this.arabic = arabic;
    }

    public int getValue() {
        return value;
    }

    public boolean isArabic() {
        return arabic;
    }

    @Override
    public String toString() {
        return isArabic() ? String.valueOf(value) : toRoman();
    }

    private String toRoman(){
        if (!canBeRoman())
            throw new RuntimeException("Error! Roman numbers can't be less than 1");

        int hundred = value/100;
        int dozen = value % 100 / 10;
        int unit = value % 10;
        return hundreds(hundred) + dozens(dozen) + units(unit);
    }

    private boolean canBeRoman() {
        return value > 0;
    }

    private String hundreds(final int hundreds) {
        switch (hundreds) {
            case 1: return "C";
            case 2: return "CC";
            case 3: return "CCC";
            case 4: return "CD";
            case 5: return "D";
            case 6: return "DC";
            case 7: return "DCC";
            case 8: return "DCCC";
        }
        return "";
    }

    private String dozens(final int dozens) {
        switch (dozens){
            case 1: return "X";
            case 2: return "XX";
            case 3: return "XXX";
            case 4: return "XL";
            case 5: return "L";
            case 6: return "LX";
            case 7: return "LXX";
            case 8: return "LXXX";
            case 9: return "XC";
        }
        return "";
    }

    private String units(final int units) {
        switch (units){
            case 1: return "I";
            case 2: return "II";
            case 3: return "III";
            case 4: return "IV";
            case 5: return "V";
            case 6: return "VI";
            case 7: return "VII";
            case 8: return "VIII";
            case 9: return "IX";
        }
        return "";
    }

}
