package org.amsmirnov;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    static final ArrayList<Character> OPERATIONS = new ArrayList<>(Arrays.asList('+', '-', '*', '/'));

    static char OPERATOR;
    static NumberRomanArabic NUM1, NUM2;

    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        while (input != null && !input.isEmpty()) {
            System.out.println(calc(input));
            input = scanner.nextLine();
        }
    }

    public static String calc(final String input) {
        parseExpression(input);

        final NumberRomanArabic result;
        switch (OPERATOR) {
            case '+':
                result = new NumberRomanArabic(NUM1.getValue() + NUM2.getValue(), NUM1.isArabic());
                break;
            case '-':
                result = new NumberRomanArabic(NUM1.getValue() - NUM2.getValue(), NUM1.isArabic());
                break;
            case '*':
                result = new NumberRomanArabic(NUM1.getValue() * NUM2.getValue(), NUM1.isArabic());
                break;
            case '/':
                result = new NumberRomanArabic(NUM1.getValue() / NUM2.getValue(), NUM1.isArabic());
                break;
            default:
                throw new RuntimeException("Error! Unknown operation");
        }
        return result.toString();
    }

    private static void parseExpression(final String expression) {
        long count = expression.chars()
                .mapToObj(c -> (char) c)
                .filter(OPERATIONS::contains)
                .count();
        if (count != 1)
            throw new RuntimeException("Error! Calculator can work only with 1 operator of : +, -, *, /");

        OPERATOR = expression.chars()
                .mapToObj(c -> (char) c)
                .filter(OPERATIONS::contains)
                .findFirst()
                .get();

        String[] operators = expression.split("[-+*/]");

        NUM1 = new NumberRomanArabic(operators[0].trim());
        NUM2 = new NumberRomanArabic(operators[1].trim());
        if (NUM1.isArabic() != NUM2.isArabic())
            throw new RuntimeException("Error! Calculator can work only with numbers from ONE numeric system");
    }

}
