package org.amsmirnov;

import org.junit.Assert;
import org.junit.Test;

public class AppTest {

    @Test
    public void Test1() {
        Assert.assertEquals(Main.calc("1 + 1"), "2");
    }

    @Test
    public void Test2() {
        Assert.assertEquals(Main.calc("VI / III"), "II");
    }

    @Test(expected = RuntimeException.class)
    public void Test3() {
        Main.calc("I - II");
    }

    @Test(expected = RuntimeException.class)
    public void Test4() {
        Main.calc("I + 1");
    }

    @Test(expected = RuntimeException.class)
    public void Test5() {
        Main.calc("1");
    }

    @Test(expected = RuntimeException.class)
    public void Test6() {
        Main.calc("1 + 2 + 3");
    }

}
